import shelve

# initalize shelf database
def initialize():

	variables = shelve.open('shelf.db')
	try:
            # These are the variables to populate the database with 
	    variables['key1'] = { 
			'usb': 'enabled',
			'string':'Sample data' }
	    print variables['key1'] 
	finally:
	    variables.close()

# read shelf database
# params:  string, "the name of variable"
# returns: string, "value of the variable"
def read_database(var):

	variables = shelve.open('shelf.db')
	try:
	    existing = variables['key1']
	finally:
	    variables.close()

	#print existing
	#print 'USB feature = ' + existing['usb']
	return existing[var]


# write variable to database
# 2 params
#	1.  variable to edit 
#	2.  new value  
# returns: nothing 
def write_database(var, new_val):

	variables = shelve.open('shelf.db', writeback=True)
	try:
	    print variables['key1']
	    #s['key1']['new_value'] = 'this has been changed'
	    variables['key1'][str(var)] = str(new_val)
	    print variables['key1']
	finally:
	    variables.close()

	variables = shelve.open('shelf.db', writeback=True)
	try:
	    print variables['key1']
	finally:
	    variables.close()

### MAIN ###

print "Initializing Database, \'shelf.db\'" 
initialize()

# read value of variable 'usb'
print read_database('usb') 

# change variable 'usb', to 'disabled'
print write_database('usb', 'disabled')

# read the value of the varialbe 'usb'
print read_database('usb')
